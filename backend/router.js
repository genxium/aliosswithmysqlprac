const koaRouter = require('koa-router');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');

const {sequelize, OssResource, Op, whereClause}  = require('./db');
const OSS = require('./OSS');
const constant = require('./constant');
const config = require('./config');

const router = new koaRouter();

router.use(async (ctx, next) => {
  try {
    await next();
  } catch (e) {
    ctx.response.body = { ret: 1001, msg: 'error occurs', };
    console.log(e);
  }
})

async function uploadFile(ctx, next) {
  let {path, name}  = ctx.request.files.image;
  let {resourcePath=name} = ctx.request.body;
  let result = await OSS.uploadPrivateObjectByStream(
    fs.createReadStream(path),
    resourcePath
  );
  console.log('OSS RESPONSE:', result); 
  let doc = await OssResource.create({
    resourcePath: resourcePath, 
    state: constant.UNSOLIDIFIED,
    metaType: constant.SOCIAL_IDENDITY_VERIFICATION,
    metaId: 0,
  });
  console.log('DOC:', doc);
  ctx.response.body = {
    ossResponse: result,
    doc: doc,
  };
}

async function solidifyObject(ctx, next) {
  let {remoteFilePath, action} = ctx.request.body;
  if (action == 'solidify') {
    let doc = await OssResource.findOne({
      where: {
        ...whereClause.STATE.UNSOLIDIFIED,
        resourcePath: { [Op.eq]: remoteFilePath },
      },
    });
    if (!doc) {
      ctx.response.body = {
        ret: 1001,
        msg: `solidify rejected: ${remoteFilePath}`,
      };
    } else {
      doc.state = constant.SOLIDIFIED;
      doc.updatedAt = Date.now();
      await doc.save();
      ctx.response.body = {
        ret: 1000,
      };
    }
  } else {
    ctx.response.body = {              
      ret: 1001,
      msg: `unknown action: ${action}`,
    };
  }
}

async function deleteObject(ctx, next) {
  let {remoteFilePath} = ctx.request.query;

  let transaction = await sequelize.transaction();
  try {
    let doc = await OssResource.findOne({
      where: {
        ...whereClause.ALIVE,
        resourcePath: { [Op.eq]: remoteFilePath },
      },
    }, {
      transaction: transaction,
    });
    if (!doc) {
      throw new Error();
    }
    let result = await OSS.deleteResource(
      remoteFilePath
    );
    console.log('OSS RESPONCE: ', result);
    doc.deletedAt = Date.now();
    await doc.save({
      transaction: transaction,
    }); 
    await transaction.commit();
    ctx.response.body = {
      ret: 1000,
    };
  } catch(e) {
    await transaction.rollback();
    ctx.response.body = { ret: 1001, msg: `delete rejected: ${remoteFilePath}` };
  }
}

async function obtainSTSToken(ctx, next) {
  // 1. create exactly 4 sql records which state==UNSOLIDIFIED && metaType == HF_BOOKING_END_PROOF
  // 2. assumeRole with these 4 resourcePath which created according to 4 sql records's id
  // 3. send the sts token to client
  const transaction = await sequelize.transaction();
  try {
    let values = [], ENDPROOF_RESOURCE_MAX_LENGTH = constant.ENDPROOF_RESOURCE_MAX_LENGTH;
    for (let i = 0; i < ENDPROOF_RESOURCE_MAX_LENGTH; i++) {
      values.push({
        state: constant.UNSOLIDIFIED,
        metaType: constant.HF_BOOKING_END_PROOF,
        resourcePath: ''
      });
    }

    // TODO: too many query in an operation, thus the sql query should be optimized
    let docs = await OssResource.bulkCreate(values, {
      transaction: transaction,
    });
    for (var doc of docs) {
      // Warning: it should has the same format, eg, begin with '/'
      // Warning: the extname is jpg
      doc.resourcePath = `/HF_BOOKING_END_PROOF_${doc.id}.jpg`;
      await doc.save({
        transaction: transaction,
      });
    }
    let resourcePaths = docs.map(doc => doc.resourcePath);
    let assumeRoleResponse = await OSS.assumeRole(resourcePaths, 'HF_BOOKING_END_PROOF');
    console.log('assumeRole response:', assumeRoleResponse);
    await transaction.commit();
    ctx.response.body = { 
      ret: 1000,
      credentials: assumeRoleResponse.credentials,
      region: config.alioss.ALI_SDK_STS_REGION,
      bucket: config.alioss.ALI_SDK_STS_BUCKET,
      allowResourcePaths: resourcePaths,
    };
  } catch (e) {
    await transaction.rollback();
    ctx.response.body = { ret: 1001, msg: `obtain STSToken failed.` };
  }
}

async function solidifyEndProofSubmit(ctx, next) {
  let { region, bucket, reservedOldResourcePaths=[], newResourcePaths=[] } = ctx.request.body;
  // Warning: if reservedOldResourcePaths has only one element, it will be string but not array
  // Warning: newResourcePaths is same
  reservedOldResourcePaths = [].concat(reservedOldResourcePaths);
  newResourcePaths = [].concat(newResourcePaths);

  if (reservedOldResourcePaths.length + newResourcePaths.length > constant.ENDPROOF_RESOURCE_MAX_LENGTH) {
    ctx.response.body = { ret: 1001, msg: `too many resource received.` };
    return await next();
  }
  let sameValues = _.intersection(reservedOldResourcePaths, newResourcePaths);
  if (sameValues.length) {
    ctx.response.body = { ret: 1001, msg: `some value duplicated.`, arr: sameValues };
    return await next();
  }
  // 1. get the old resouce
  // 2. delete the unreservedOldResources
  // 3. 'delete' the unreservedOldResources record in db
  // 4. 'solidify' the newResources record in db
  const transaction = await sequelize.transaction();
  try {
    let toDeleteDocs = await OssResource.findAll({
      where: {
        ...whereClause.STATE.SOLIDIFIED,
        ...whereClause.METATYPE.HF_BOOKING_END_PROOF,
        resourcePath: {
          [Op.notIn]: reservedOldResourcePaths
        },
      },
    }, {
      transaction: transaction,
    });
    for (let doc of toDeleteDocs) {
      await OSS.deleteResource(doc.resourcePath);
      doc.deletedAt = Date.now();
      doc.save({
        transaction: transaction
      });
    }
    let solidifyResult = await OssResource.update({
      state: constant.SOLIDIFIED,
      updatedAt: Date.now(),
    },{
      where: {
        ...whereClause.STATE.UNSOLIDIFIED,
        ...whereClause.METATYPE.HF_BOOKING_END_PROOF,
        resourcePath: {
          [Op.in]: newResourcePaths
        },
      },
      transaction: transaction,
    });
    await transaction.commit();
    ctx.response.body = {
      ret: 1000,
      deleteRows: toDeleteDocs.length,
      solidifidResult: solidifyResult,
    };
  } catch(e) {
    await transaction.rollback();
    throw e;
  }
}

async function listEndProofResource(ctx, next) {
  let docs = await OssResource.findAll({
    where: {
      ...whereClause.STATE.SOLIDIFIED,
      ...whereClause.METATYPE.HF_BOOKING_END_PROOF,
    },
  });
  ctx.response.body = {
    ret: 1000,
    resourcePaths: docs.map(doc => doc.resourcePath),
  };
}

router.post('/', uploadFile);
router.put('/', solidifyObject);
router.del('/', deleteObject);
router.post('/v1/HostFeatBooking/EndProof/StsWriteToken/Obtain', obtainSTSToken);
router.post('/v1/HostFeatBooking/EndProof/Submit', solidifyEndProofSubmit);
router.post('/v1/HostFeatBooking/EndProof/List', listEndProofResource);

module.exports = router;

