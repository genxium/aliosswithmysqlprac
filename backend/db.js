const config = require('./config');
const constant = require('./constant');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const sequelize = new Sequelize(
  config.mysql.dbname,
  config.mysql.username,
  config.mysql.password,
  {
    host: config.mysql.host,
    port: config.mysql.port,
    dialect: 'mysql',
    pool: {
      max: 5,
      min: 0,
      acquire: 3000,
      idle: 1000,
    },
  },
);

const OssResource = sequelize.define('OssResource', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  resourcePath: { 
    type: Sequelize.STRING,
    field: 'resource_path',
  },
  state: Sequelize.INTEGER,
  metaType: {
    type: Sequelize.INTEGER,
    field: 'meta_type',
  },
  metaId: {
    type: Sequelize.INTEGER,
    field: 'meta_id',
  },
  createdAt: {
    type: Sequelize.BIGINT,
    field: 'created_at',
    defaultValue: function() {
      return Date.now();
    },
  },
  updatedAt: {
    type: Sequelize.BIGINT,
    field: 'updated_at',
    defaultValue: function() {
      return Date.now();
    },
  },
  deletedAt: {
    type: Sequelize.BIGINT,
    field: 'deleted_at',
    defaultValue: null,
  },
}, {
  tableName: 'oss_resource',
  timestamps: false,
});

const whereClause = {
  STATE: {
    UNSOLIDIFIED: {
      state: {
        [Op.eq]: constant.UNSOLIDIFIED,
      },
    },
    SOLIDIFIED: {
      state: {
        [Op.eq]: constant.SOLIDIFIED,
      }
    },
  },
  METATYPE: {
    HF_BOOKING_END_PROOF: {
      metaType: {
        [Op.eq]: constant.HF_BOOKING_END_PROOF,
      },
    },
    SOCIAL_IDENDITY_VERIFICATION: {
      metaType: {
        [Op.eq]: constant.SOCIAL_IDENDITY_VERIFICATION,
      }
    },
  },
  ALIVE: {
    deletedAt: {
      [Op.eq]: null
    }
  },
};

// Warning: these whereClause don't care those deletedAt is not null
const ALIVE = {
  deletedAt: {
    [Op.eq]: null
  },
};

for (let clause of  Object.values(whereClause)) {
  for (let obj of Object.values(clause)) {
    Object.assign(obj, ALIVE);
  }
}

module.exports = {
  sequelize,
  OssResource,
  Op,
  whereClause,
}

