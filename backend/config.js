const fs = require('fs');
const path = require('path');
const yaml = require('js-yaml');

const configDirectoryRoot = path.join(__dirname, 'configs');

function loadAllConfFile() {
  let files = fs.readdirSync(configDirectoryRoot);
  files.forEach((x, i) => {
    let extName = path.extname(x), filePath = path.join(configDirectoryRoot, x);
    if ( /^\.conf$/i.test(extName) ) {
      let configField = x.match(/^(\w+)\./)[1];
      if ( /^\.test.conf$/i.test(x.slice(-10)) ) {
        configForTest[configField] = yaml.safeLoad(fs.readFileSync(filePath));
      } else {
        config[configField] = yaml.safeLoad(fs.readFileSync(filePath));
      }
      console.log('load conf file:', x);
    } else {
      return;
    }
  })
}

const configForTest = {};

const config = {};

loadAllConfFile();

const obj = {
  init: function(IS_TESTING) {
    Object.assign(obj, IS_TESTING ? configForTest : config);
    obj.IS_TESTING = !!IS_TESTING;
    return obj;
  },
};

module.exports = obj;

