const koa = require('koa');
const koaBody = require('koa-body');
const koaLogger = require('koa-logger');

void async function(IS_TESTING=false, PORT=8888) {

// config should be inited at the beginning
const config = require('./config.js').init(IS_TESTING);

const {sequelize} = require('./db');
const router = require('./router');

const app = new koa();

app.use(koaLogger());
app.use(koaBody({ multipart: true }));

app.use(router.routes());

try {
  await sequelize.authenticate();
  console.log('Connection has been established successfully.');
  console.log('server listen on Port: ', PORT);
  app.listen(PORT);

} catch(e) {
  console.log('Unable to connect to the database:', e);
}

}(process.env.TESTING, process.env.PORT)

