const config = require('./config').init(process.env.IS_TESTING);
const { sequelize, OssResource, Op } = require('./db');
const constant = require('./constant');
const OSS = require('./OSS');

async function tickHandler() {
  let now = Date.now(), minTimeStamp = now - config.scheduler.THRESHOLD_SECONDS * 1000;
  let transaction = await sequelize.transaction();
  console.log('scheduler tick at', now, ' <-> ', new Date(now).toLocaleString());
  console.log('delete target created before', minTimeStamp, ' <-> ',  new Date(minTimeStamp).toLocaleString());
  try {
    let docs = await OssResource.findAll({
      where: {
        state: {
          [Op.eq]: constant.UNSOLIDIFIED,
        },
        createdAt: {
          [Op.lte]: minTimeStamp,
        },
        deletedAt: {
          [Op.eq]: null,
        },
      },
    }, {
      transaction: transaction
    });
    for (let doc of docs) {
      let result = await OSS.deleteResource(config.scheduler.REGION, doc.resourcePath);
      console.log('delete doc:', doc.toJSON());
      doc.deletedAt = now;
      await doc.save({
        transaction: transaction
      });
    }
    console.log(`update ${docs.length} rows.`);
    await transaction.commit();
  } catch(err) {
    await transaction.rollback();
    throw err;
  }
}
const TICK_INTERVAL_MILLS = config.scheduler.TICK_INTERVAL * 1000;

async function tick() {
  console.log("------------------");
  try {
    await tickHandler();
  } catch(err) {
    console.log(err);
  }
  console.log("------------------\n");
  setTimeout(tick, TICK_INTERVAL_MILLS);
}

setTimeout(tick, TICK_INTERVAL_MILLS);

