const path = require('path');
const OSS = require('ali-oss');
const STS = OSS.STS;
const config = require('./config');
const constant = require('./constant');

const sts = new STS({
  accessKeyId: config.alioss.ALI_SDK_STS_ID,
  accessKeySecret: config.alioss.ALI_SDK_STS_SECRET
});

function getClient() {
  return new OSS({
    region: config.alioss.ALI_SDK_STS_REGION,
    accessKeyId: config.alioss.ALI_SDK_STS_ID,
    accessKeySecret: config.alioss.ALI_SDK_STS_SECRET,
    bucket: config.alioss.ALI_SDK_STS_BUCKET,
  });
}

async function uploadPrivateObjectByStream(stream, objectName) {
  let client = getClient();
  let result = await client.putStream(objectName, stream, {
    // Be awared that headers has a 's'
    headers: {
      'x-oss-object-acl': 'private',
    },
  });
  let aclResult = await client.getACL(objectName);
  if (aclResult.acl !== 'private') {
    throw new Error(`acl ircorrect: ${aclResult.acl}, required is 'private'`);
  }
  return result;
}

async function deleteResource(objectName) {
  let client = getClient();
  return await client.delete(objectName);
}

// sessionName may not be the same?
async function assumeRole(resourcePath, sessionName) {
  // Warning: each resourcePath must begin with a '/'
  // Warning: resourcePath.length should lte 4
  let Resource = resourcePath.map(x => `acs:oss:*:*:${config.alioss.ALI_SDK_STS_BUCKET}${x}`);
  let policy = {
    Statement: [
      {
        Action: [
          "oss:PutObject",
          "oss:GetObject"
        ],
        Effect: "Allow",
        Resource
      }
    ],
    Version: "1", // Warning: the Version must be string
  };
  return await sts.assumeRole(config.alioss.ALI_SDK_STS_ROLE, policy, constant.TOKEN_REMAIN_SECONDS, sessionName);
}

module.exports = {
  uploadPrivateObjectByStream, deleteResource, assumeRole,
  getClient,
}

