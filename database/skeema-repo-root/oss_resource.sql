CREATE TABLE `oss_resource` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `resource_path` varchar(64) DEFAULT NULL COMMENT 'A string indicating the fullpath w.r.t. bucket root, e.g. "/dog.png", "/<dirname>/cat.png", "/<dirname>/<anotherDirname>/elephant.jpg"',
  `state` int(20) unsigned NOT NULL DEFAULT 0 COMMENT 'UNSOLIDIFIED: 0, SOLIDIFIED: 1',
  `meta_type` int(20) unsigned DEFAULT NULL COMMENT 'An integer indicating record usage for indexing, SOCIAL_IDENDITY_VERIFICATION: 0, HOST_FEAT_BINDING: 1, HOST_FEAT_BOOKING: 2',
  `meta_id` int(20) unsigned DEFAULT NULL,
  `created_at` bigint(32) unsigned NOT NULL,
  `updated_at` bigint(20) unsigned NOT NULL,
  `deleted_at` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
