This repository for internal training use only.

# 1. Operating System

The backend part of this project is assumed to be running on Ubuntu 14.04 LTS.  

# 2. Database Server

The database product to be used for this project is MySQL 5.7.

We use [skeema](https://github.com/skeema/skeema) for schematic synchronization under `<proj-root>/database/skeema-repo-root/` which intentionally doesn't contain a `.skeema` file. Please read [this tutorial](https://shimo.im/doc/wQ0LvB0rlZcbHF5V) for more information.

With `skeema version 0.2` and `golang version 1.10`, it's tested to be compilable on `Windows10`, but NOT useful due to the failure of trial to communicate via `UNIX socket`.
```
powershell> skeema.exe --version
skeema version 0.2 (beta)

powershell> go version
go version go1.10 windows/amd64
```

You can use [this node module (still under development)](https://github.com/genxium/node-mysqldiff-bridge) instead under `Windows10`, other versions of Windows are not yet tested for compatibility.

The following command(s)
```
### Optional.
user@proj-root/database/skeema-repo-root> cp .skeema.template .skeema

###
user@proj-root/database/skeema-repo-root> skeema diff
```
is recommended to be used for checking difference from your "live MySQL server" to the latest expected schema tracked in git.

## 2.1 Environmental differentiation

Please note that we have 2 candidate live MySQL server configurations `<proj-root>/backend/configs/mysql.conf` and `<proj-root>/backend/configs/mysql.test.conf`, where the latter would only be used if `process.env.TESTING == "true"` for your NodeJs process(es) that runs the server.

# 3. What & How to Install

## 3.1 NodeJs Runtime

Please install `NodeJs 10.x` dependencies by [Ubuntu14InitScripts/backend/node/init](https://github.com/genxium/Ubuntu14InitScripts/tree/master/backend/node).

## 3.2 MySQL 

On a product machine, you can install and manage `MySQL` server by [these scripts](https://github.com/genxium/Ubuntu14InitScripts/tree/master/database/mysql).

## 3.3 Node Modules

Please run

```
proj-root/backend> npm install
proj-root/frontend> npm install
```

to complete the installation.

## 3.4 Required Config Files

Please make sure that the following config files 
```
- <proj-root>/backend/configs/mysql.test.conf, used by the api-server in non-production modes
- <proj-root>/backend/configs/mysql.conf, used by the api-server in production mode
- <proj-root>/backend/configs/alioss.test.conf, used by the api-server in non-production modes
- <proj-root>/backend/configs/alioss.conf, used by the api-server in production mode
```
exist and are properly set **before starting the api daemon under ANY mode**.

Try

```
proj-root/backend> ./overwrite_configs 
```

or do it manually under `Windows10` (other versions of Windows are not yet tested). 

Regarding the details of `alioss[.test].conf`, please refer to the [README of this repository](https://git.red0769.com/lock-interactive/ali-oss-nodejs/src/branch/master/example).

## 3.5 Redis-server
Launching a redis-server before **before starting the api daemon under ANY mode** is required. You can install and manage `Redis` server by [these scripts](https://github.com/genxium/Ubuntu14InitScripts/tree/master/database/redis).

## 3.7 How to Start & Stop Backend Service (single process assumed) 

You might have to update the `<proj-root>/backend/configs/*.conf` files to match valid credentials.

Start the backend service process.
```
proj-root> node backend/api_server.js
```

## 3.8 How to Launch Frontend by Feature
```
proj-root/frontend> node launcher.js --Endpoint localhost:8888 --Action upload --AbsFilePath /path/to/OriginalImageName.jpg
proj-root/frontend> node launcher.js --Endpoint localhost:8888 --Action solidify --RemoteFilePath /Remote/Resource/Path.jpg
proj-root/frontend> node launcher.js --Endpoint localhost:8888 --Action delete --RemoteFilePath /Remote/Resource/Path.jpg
```

If you're just beginning with this practice, just omit the "/Remote/Resource" prefix in "/Remote/Resource/Path.jpg", i.e. put all your uploaded files right at the root directory under the configured `bucket`.

By uploading, please add the http header field `x-oss-object-acl: private` to set the object scope access, according to the following reference docs. 
- https://help.aliyun.com/document_detail/31867.html?spm=5176.11065259.1996646101.searchclickresult.4b1c4b31WLHmKh
- https://help.aliyun.com/document_detail/31978.html

After uploaded, use [GetObject](https://help.aliyun.com/document_detail/31980.html) and [GetObjectAcl](https://help.aliyun.com/document_detail/31987.html?spm=a2c4g.11186623.4.6.363c20beNvaNz0) to verify whether all fields of the targeted object are set as expected.

## 3.9 Cleaning UNSOLIDIFIED `oss_resource` records

You are supposed to write a scheduler which runs in a process other than the backend process a.k.a. `api_server`, with a tick-interval 5 seconds. At each tick, it should delete UNSOLIDIFIED `oss_resource` records whose `now() - updated_at > {THRESHOLD_SECONDS}`.    

The scheduler script entry should be located at `<proj-root>/backend/scheduler.js`, and please use MySQL connection pool to reduce redundant connections with it.

## 3.10 Batch photo replacement for a specific Feature `upsert` 
### 3.10.1 Cmd specs
```
proj-root/frontend> node launcher.js --Endpoint localhost:8888 --Action discover 
proj-root/frontend> node launcher.js --Endpoint localhost:8888 --Action upsert --reservedOldResourcePaths /HF_BOOKING_END_PROOF_{ST_OSS_RESOURCE_ID_SOLIDIFIED1}.jpg --reservedOldResourcePaths /HF_BOOKING_END_PROOF_{ANOTHER_ST_OSS_RESOURCE_ID_SOLIDIFIED2}.jpg --AbsFilePathList /path/to/OriginalImageName1.jpg --AbsFilePathList /path/to/OriginalImageName2.jpg
```

Invocation of `--Action discover` should print out all records sufficing `oss_resource.state == SOLIDIFIED & meta_type == HF_BOOKING_END_PROOF` queried from MySQL-server. 

Invocation of `--Action upsert` should call `the following APIs(#3.10.2 ~ #3.10.4) in order` to create or update the batch of resources right under the root dir of the configured bucket.

### 3.10.2 BatchResourceSTSWriteTokenObtain
#### Request
|K                    | V                                                                 |
---                   | ---                                                               |
|method               | POST                                                              |
|Requested Http Route | /v1/HostFeatBooking/EndProof/StsWriteToken/Obtain                 |
|Content-Type         | application/x-www-form-urlencoded                                 |

#### Response
|K                    | V                                                                 |
---                   |:---                                                               |
|Content-Type         | application/json                                                  |
|Body                 | {<br>&nbsp;&nbsp;ret, <br>&nbsp;&nbsp;credentials: {&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;AccessKeySecret,<br>&nbsp;&nbsp;&nbsp;&nbsp;AccessKeyId,<br>&nbsp;&nbsp;&nbsp;&nbsp;Expiration,<br>&nbsp;&nbsp;&nbsp;&nbsp;SecurityToken<br>&nbsp;&nbsp;},<br>&nbsp;&nbsp;region,<br>&nbsp;&nbsp;bucket,<br>&nbsp;&nbsp;allowResourcePaths: [...]<br>} |

- 此API成功调用后，应创建新的oss_resource.state == UNSOLIDIFIED & meta_type == HF_BOOKING_END_PROOF记录，但不要立刻删除对应该host_feat_booking的原有oss_resource.state == SOLIDIFIED记录！
- The responded field "allowResourcePaths" must contain exactly 4 strings 
    - `"/HF_BOOKING_END_PROOF_{ST_OSS_RESOURCE_ID}.jpg"`, 
    - `"/HF_BOOKING_END_PROOF_{ST_OSS_RESOURCE_ID+AT_LEAST_ONE}.jpg"`, 
    - `"/HF_BOOKING_END_PROOF_{ST_OSS_RESOURCE_ID+AT_LEAST_TWO}.jpg"`, 
    - `"/HF_BOOKING_END_PROOF_{ST_OSS_RESOURCE_ID+AT_LEAST_THREE}.jpg"` 
    
    where {ST_OSS_RESOURCE_ID} is the first inserted `oss_resource.id` by each call of this API, and that due to possibly concurrent insertion the group of `oss_resource.id` inserted by the same SQLTransaction might not be consecutive, thus comes the `AT_LEAST_ONE`, `AT_LEAST_TWO`, `AT_LEAST_THREE` remarks. 

### 3.10.3 OSS "PutObject"
Use the OSS "PutObject" API to upload all resources in your batch with the allocated UNSOLIDIFIED records, e.g. `"/HF_BOOKING_END_PROOF_14.jpg", "/HF_BOOKING_END_PROOF_15.jpg", "/HF_BOOKING_END_PROOF_16.jpg" and "/HF_BOOKING_END_PROOF_17.jpg"` from `#3.10.2`.

### 3.10.4 BatchResourceSolidify
#### Request
|K                    | V                                                                 |
---                   |:---                                                               |
|method               | POST                                                              |
|Requested Http Route | /v1/HostFeatBooking/EndProof/Submit                               |
|Content-Type         | application/x-www-form-urlencoded                                 |
|Body                 | {<br>&nbsp;&nbsp;region,<br>&nbsp;&nbsp;bucket,<br>&nbsp;&nbsp;reservedOldResourcePaths,<br>&nbsp;&nbsp;newResourcePaths<br>} |

- There should be no intersection between "reservedOldResourcePaths" and "newResourcePaths".
- The requested field "reservedOldResourcePaths" must contain at most 4 strings "/HF_BOOKING_END_PROOF_{ST_OSS_RESOURCE_ID_SOLIDIFIED}.jpg", where {ST_OSS_RESOURCE_ID_SOLIDIFIED} is any SOLIDIFIED `oss_resource.id` known to you. 
- The requested field "newResourcePaths" must contain at most 4 strings `"/HF_BOOKING_END_PROOF_{ST_OSS_RESOURCE_ID_UNSOLIDIFIED}.jpg"`, where {ST_OSS_RESOURCE_ID_UNSOLIDIFIED} is any UNSOLIDIFIED `oss_resource.id` known to you. 



#### Response
|K                    | V                                                                 |
---                   |:---                                                               |
|Content-Type         | application/json                                                  |
|Body                 | {<br>&nbsp;&nbsp;ret<br>}                                         |

此API调用过程中，应(within a same SQL transaction)
- 先删除OSS上对应`oss_resource.state == SOLIDIFIED & meta_type == HF_BOOKING_END_PROOF & resource_path NOT IN (<reservedOldResourcePaths>)`的图片资源.
    - 注意，必须是先尝试删除OSS资源再删除数据库记录，这样即使在此步执行时服务器断电再上电，用户依然可以再次调用本API完成原意图的操作.
- 再删除数据库中`oss_resource.state == SOLIDIFIED & meta_type == HF_BOOKING_END_PROOF & resource_path NOT IN (<reservedOldResourcePaths>)`的记录.
- 更新数据库中`oss_resource.state == UNSOLIDIFIED & meta_type == HF_BOOKING_END_PROOF & resource_path IN (<newResourcePaths>)`的记录到state = SOLIDIFIED.

