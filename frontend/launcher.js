const program = require('commander');
const rq = require('request-promise-native');
const fs = require('fs');
const path = require('path');
const OSS = require('ali-oss');

function parseToList(val) {
  return val.slice(1).split(/, ?/);
}

function collect(val, memo) {
  memo.push(val);
  return memo;
} 

program.version('0.0.1')
  .option('-E, --Endpoint <endpoint>', 'the server point', 'localhost:8888')
  .option('-A, --Action <action>', 'the action to do', /^upload|solidify|delete|upsert|discover$/i, 'upload')
  .option('-F, --AbsFilePath <file>', 'the local file to be operated')
  .option('-L, --AbsFilePathList [filePathList]', 'the local files to upsert', collect, [])
  .option('-O, --reservedOldResourcePaths [objectPathList]', 'the object path to reserve', collect, [])
  .option('-R, --RemoteFilePath <remote>', 'the remote path to be operated')
  .parse(process.argv);

if (/upload/i.test(program.Action)) {
  program.Target = program.AbsFilePath;
} else if (/upsert/i.test(program.Action)) {
  program.Target = program.AbsFilePathList;
} else if (/discover/i.test(program.Action)) {
  program.Target = "HF_BOOKING_END_PROOF";
} else {
  program.Target = program.RemoteFilePath;
}

if (!program.Target) {
  console.log('require --AbsFilePath for upload otherwise --RemoteFilePath');
  process.exit(0);
}

const {Endpoint} = program;

void async function doAction(action, target) {
  let res;
  console.log(`${action} with: ${target}`);
  if (/upload/i.test(action)) {
    res = await uploadFile(target);
  } else if (/solidify/i.test(action)) {
    res = await solidify(target);
  } else if (/delete/i.test(action)) {
    res = await deleteResource(target);
  } else if (/upsert/i.test(action)) {
    res = await upsertResource([target, program.reservedOldResourcePaths]);
  } else if (/discover/i.test(action)) {
    res = await discover(target);
  } else {
    console.warn('unknown action:', action);
    return;
  }
  console.log('response:');
  console.log(res);
}(program.Action, program.Target);

async function uploadFile(absFile) {
  let extName = path.extname(absFile);
  let resourcePath = `${path.basename(absFile).replace(extName, '')}_${Date.now().toString(16)}${extName}`;
  let option = {
    url: 'http://' + Endpoint,
    formData: {
      image: fs.createReadStream(absFile),
      resourcePath,
    },
    json: true,
  }
  return await rq.post(option);
}

async function solidify(target) {
  let option = {
    url: 'http://' + Endpoint,
    formData: {
      remoteFilePath: target,
      action: 'solidify',
    },
    json: true,
  };
  return await rq.put(option);
}

async function deleteResource(target) {
  let option = {
    url: 'http://' + Endpoint,
    qs: {
      remoteFilePath: target,
    },
    json: true,
  };
  return await rq.delete(option);
}

async function discover(target) {
  let option = {
    url: 'http://' + Endpoint + '/v1/HostFeatBooking/EndProof/List',
    formData: {},
    json: true,
  }
  return await rq.post(option);
}

async function upsertResource([filePathList=[], reservedOldResourcePaths=[]]) {
  // Warning: filePathList.length + reservedOldResourcePaths.length must lte `../backend/constant`.ENDPROOF_RESOURCE_MAX_LENGTH
  let obtainOption = {
    url: 'http://' + Endpoint + '/v1/HostFeatBooking/EndProof/StsWriteToken/Obtain',
    json: true,
  };
  let token = await rq.post(obtainOption);
  // console.log('obtain STS Token', token);
  let client = new OSS({
    region: token.region,
    bucket: token.bucket,
    accessKeyId: token.credentials.AccessKeyId,
    accessKeySecret: token.credentials.AccessKeySecret,
    stsToken: token.credentials.SecurityToken,
  });
  let allowResourcePaths = token.allowResourcePaths;
  let len = Math.min(filePathList.length, allowResourcePaths.length);
  for (let i = 0; i < len; i++) {
    let filePath = filePathList[i], resourcePath = allowResourcePaths[i];
    await client.put(resourcePath, filePath);
  }
  let newResourcePaths = allowResourcePaths.slice(0, len);
  let upsertSolidifyOption = {
    url: 'http://' + Endpoint + '/v1/HostFeatBooking/EndProof/Submit',
    formData: {
      region: token.region,
      bucket: token.bucket,
      reservedOldResourcePaths,
      newResourcePaths,
    },
    json: true,
  };
  let res = await rq.post(upsertSolidifyOption);
  return {
    token, res
  };
}

